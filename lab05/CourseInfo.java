//Severo Lopez
//4 Oct 2018
//CSE002-311
import java.util.Scanner; //allow you to use a scanner to ask for input
public class CourseInfo{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner scan = new Scanner( System.in ); //creates a new scanner
    System.out.print("Course Number: ");
    int courseNum = -1;
    boolean correct = false;
    while (!correct){
    	if (!scan.hasNextInt()){
    		scan.next();
    		System.out.println("Invalid input. Please enter an integer: ");
    	} else {
    		courseNum = scan.nextInt();
    		if (courseNum < 0) {
    			System.out.print("Invalid input. Please enter a positive integer: ");
    		} else {
    			correct = true;
    		}
    	}
    }

    System.out.print("Department: ");
    String department = "";
    correct = false;
    while (!correct){
    	if (!scan.hasNext()){
    		scan.next();
    		System.out.print("Invalid input. Please enter an string: ");
    	} else {
    		department = scan.next();
    		correct = true;
    	}
    }

    System.out.print("Number of times per week: ");
    String week = "";
    correct = false;
    while (!correct){
    	if (!scan.hasNext()){
    		scan.next();
    		System.out.print("Invalid input. Please enter an string: ");
    	} else {
    		week = scan.next();
    		correct = true;
    	}
    }

    System.out.print("Start time (hours): ");
    int hours = -1;
    correct = false;
    while (!correct){
    	if (!scan.hasNextInt()){
    		scan.next();
    		System.out.println("Invalid input. Please enter an integer: ");
    	} else {
    		hours = scan.nextInt();
    		if (hours < 0) {
    			System.out.print("Invalid input. Please enter a positive integer: ");
    		} else {
    			correct = true;
    		}
    	}
    }

    System.out.print("Start time (minutes): ");
    int minutes = -1;
    correct = false;
    while (!correct){
    	if (!scan.hasNextInt()){
    		scan.next();
    		System.out.println("Invalid input. Please enter an integer: ");
    	} else {
    		minutes = scan.nextInt();
    		if (hours < 0) {
    			System.out.print("Invalid input. Please enter a positive integer: ");
    		} else {
    			correct = true;
    		}
    	}
    }

    System.out.print("Instructor name: ");
    String name = "";
    correct = false;
    while (!correct){
    	if (!scan.hasNext()){
    		scan.next();
    		System.out.print("Invalid input. Please enter an string: ");
    	} else {
    		name = scan.next();
    		correct = true;
    	}
    }

    System.out.print("Number of students: ");
    int numStudents = -1;
    correct = false;
    while (!correct){
    	if (!scan.hasNextInt()){
    		scan.next();
    		System.out.println("Invalid input. Please enter an integer: ");
    	} else {
    		numStudents = scan.nextInt();
    		if (hours < 0) {
    			System.out.print("Invalid input. Please enter a positive integer: ");
    		} else {
    			correct = true;
    		}
    	}
    }
  }
}