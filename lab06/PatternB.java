//Severo Lopez
//11 Oct 2018
//CSE002-311
import java.util.Scanner;
public class PatternB{
  // main method required for every Java program
  public static void main(String[] args) {
  	Scanner scan = new Scanner(System.in);
  	System.out.print("Length of pyramid: ");
    int input = -1;
    boolean correct = false;
    while (!correct){ //checks to see if the user input an integer
    	if (!scan.hasNextInt()){
    		scan.next();
    		System.out.println("Invalid input. Please enter an integer: ");
    	} else {
    		input = scan.nextInt();
    		if (input < 0 || input > 10) { //checks to see if the user input a negative integer
    			System.out.print("Invalid input. Please enter an integer between 1 and 10: ");
    		} else {
    			correct = true;
    		}
    	}
    }
    for (int row = input; row >= 1; --row) {
        for(int col = 1; col <= row; ++col) {
            System.out.print(col + " ");
        }
        System.out.println();
    }
    }
}