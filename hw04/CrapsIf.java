//Severo Lopez
//23 Sep 2018
//CSE002-311
import java.util.Scanner; //allow you to use a scanner to ask for input
public class CrapsIf{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //creates a new scanner
    System.out.println("Enter 1 to randomly generate dice, enter 2 to input dice roll: ");
    int roll = myScanner.nextInt(); //user is asked to choose between random roll or to input roll
    if (roll == 1){ //random roll
      int dice1 = (int) (Math.random() * 6) + 1; //variable for dice 1
      int dice2 = (int) (Math.random() * 6) + 1; //variable for dice 2
      if (dice1==dice2){ //if statement for die rolls where the two dice have the same number
        if (dice1==1){
          System.out.println("Snake Eyes");
        }else if (dice1==2){
          System.out.println("Hard Four");
        }else if (dice1==3){
          System.out.println("Hard Six");
        }else if (dice1==4){
          System.out.println("Hard Eight");
        }else if (dice1==5){
          System.out.println("Hard Ten");
        }else if (dice1==6){
          System.out.println("Boxcars");
        }
      }else if (dice1+dice2==3){ //if statement for die rolls that add to 3
        System.out.println("Ace Deuce");
      }else if (dice1+dice2==5){ //if statement for die rolls that add to 5
        System.out.println("Easy Five");
      }else if (dice1+dice2==7){ //if statement for die rolls that add to 7
        System.out.println("Seven Out");
      }else if (dice1+dice2==9){ //if statement for die rolls that add to 9
        System.out.println("Nine");
      }else if (dice1+dice2==11){ //if statement for die rolls that add to 11
        System.out.println("Yo-leven");
      }else if ((dice1+dice2==4)&&(dice1!=dice2)){ //if statement for die rolls that add to 4 and aren't the same number
        System.out.println("Easy Four");
      }else if ((dice1+dice2==6)&&(dice1!=dice2)){ //if statement for die rolls that add to 6 and aren't the same number
        System.out.println("Easy Six");
      }else if ((dice1+dice2==8)&&(dice1!=dice2)){ //if statement for die rolls that add to 8 and aren't the same number
        System.out.println("Easy Eight");
      }else if ((dice1+dice2==10)&&(dice1!=dice2)){ //if statement for die rolls that add to 10 and aren't the same number
        System.out.println("Easy Ten");
      }
    }else if (roll == 2){ //input rolls
      System.out.println("Input die 1: ");
      int die1 = myScanner.nextInt(); //input roll for die 1
      System.out.println("Input die 2: ");
      int die2 = myScanner.nextInt(); //input roll for die 2
      if (die1<1 || die1>6 || die2<1 || die2>6){ //if statement for rolls that are not possible on a six sided die1
        System.out.println("Invalid die roll");
      }else if (die1==die2){
        if (die1==1){
          System.out.println("Snake Eyes");
        }else if (die1==2){
          System.out.println("Hard Four");
        }else if (die1==3){
          System.out.println("Hard Six");
        }else if (die1==4){
          System.out.println("Hard Eight");
        }else if (die1==5){
          System.out.println("Hard Ten");
        }else if (die1==6){
          System.out.println("Boxcars");
        }
      }else if (die1+die2==3){
        System.out.println("Ace Deuce");
      }else if (die1+die2==5){
        System.out.println("Easy Five");
      }else if (die1+die2==7){
        System.out.println("Seven Out");
      }else if (die1+die2==9){
        System.out.println("Nine");
      }else if (die1+die2==11){
        System.out.println("Yo-leven");
      }else if ((die1+die2==4)&&(die1!=die2)){
        System.out.println("Easy Four");
      }else if ((die1+die2==6)&&(die1!=die2)){
        System.out.println("Easy Six");
      }else if ((die1+die2==8)&&(die1!=die2)){
        System.out.println("Easy Eight");
      }else if ((die1+die2==10)&&(die1!=die2)){
        System.out.println("Easy Ten");
      }
    }
  }
}