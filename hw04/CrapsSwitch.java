//Severo Lopez
//23 Sep 2018
//CSE002-311
import java.util.Scanner;
public class CrapsSwitch{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //creates a new scanner
    System.out.println("Enter 1 to randomly generate dice, enter 2 to input dice roll: ");
    int roll = myScanner.nextInt(); //user is asked to choose between random roll or to input roll
    switch (roll){
      case 1: //random rolls
        int dice1 = (int) (Math.random() * 6) + 1; //variable for dice 1
        int dice2 = (int) (Math.random() * 6) + 1; //variable for dice 2
        switch (dice1){
          case 1: //given dice 1 roll is 1
            switch (dice2){
              case 1: //output for when dice 2 is 1
                System.out.println("Snake Eyes");
                break;
              case 2: //output for when dice 2 is 2
                System.out.println("Ace Deuce");
                break;
              case 3: //output for when dice 2 is 3
                System.out.println("Easy Four");
                break;
              case 4: //output for when dice 2 is 4
                System.out.println("Fever Five");
                break;
              case 5: //output for when dice 2 is 5
                System.out.println("Easy Six");
                break;
              case 6: //output for when dice 2 is 6
                System.out.println("Seven Out");
                break;
            }
            break;
          case 2: //given dice 1 roll is 2
            switch (dice2){
              case 1:
                System.out.println("Ace Deuce");
                break;
              case 2:
                System.out.println("Hard Four");
                break;
              case 3:
                System.out.println("Fever Five");
                break;
              case 4:
                System.out.println("Easy Six");
                break;
              case 5:
                System.out.println("Seven Out");
                break;
              case 6:
                System.out.println("Easy Eight");
                break;
            }
            break;
          case 3: //given dice 1 roll is 3
            switch (dice2){
              case 1:
                System.out.println("Easy Four");
                break;
              case 2:
                System.out.println("Fever Five");
                break;
              case 3:
                System.out.println("Hard Six");
                break;
              case 4:
                System.out.println("Seven Out");
                break;
              case 5:
                System.out.println("Easy Eight");
                break;
              case 6:
                System.out.println("Nine");
                break;
            }
            break;
          case 4: //given dice 1 roll is 4
            switch (dice2){
              case 1:
                System.out.println("Fever Five");
                break;
              case 2:
                System.out.println("Easy Six");
                break;
              case 3:
                System.out.println("Seven Out");
                break;
              case 4:
                System.out.println("Hard Eight");
                break;
              case 5:
                System.out.println("Nine");
                break;
              case 6:
                System.out.println("Easy Ten");
                break;
            }
            break;
          case 5: //given dice 1 roll is 5
            switch (dice2){
              case 1:
                System.out.println("Easy Six");
                break;
              case 2:
                System.out.println("Seven Out");
                break;
              case 3:
                System.out.println("Easy Eight");
                break;
              case 4:
                System.out.println("Nine");
                break;
              case 5:
                System.out.println("Hard Ten");
                break;
              case 6:
                System.out.println("Yo-leven");
                break;
            }
            break;
          case 6: //given dice 1 roll is 6
            switch (dice2){
              case 1:
                System.out.println("Seven Out");
                break;
              case 2:
                System.out.println("Easy Eight");
                break;
              case 3:
                System.out.println("Nine");
                break;
              case 4:
                System.out.println("Easy Ten");
                break;
              case 5:
                System.out.println("Yo-leven");
                break;
              case 6:
                System.out.println("Boxcars");
                break;
            }
            break;
        }
        break;
      case 2: //input rolls
        System.out.println("Input die 1: ");
        int die1 = myScanner.nextInt(); //input for die 1
        System.out.println("Input die 2: ");
        int die2 = myScanner.nextInt(); //input for die 2
        switch (die1){
          case 1:
            switch (die2){
              case 1:
                System.out.println("Snake Eyes");
                break;
              case 2:
                System.out.println("Ace Deuce");
                break;
              case 3:
                System.out.println("Easy Four");
                break;
              case 4:
                System.out.println("Fever Five");
                break;
              case 5:
                System.out.println("Easy Six");
                break;
              case 6:
                System.out.println("Seven Out");
                break;
            }
            break;
          case 2:
            switch (die2){
              case 1:
                System.out.println("Ace Deuce");
                break;
              case 2:
                System.out.println("Hard Four");
                break;
              case 3:
                System.out.println("Fever Five");
                break;
              case 4:
                System.out.println("Easy Six");
                break;
              case 5:
                System.out.println("Seven Out");
                break;
              case 6:
                System.out.println("Easy Eight");
                break;
            }
            break;
          case 3:
            switch (die2){
              case 1:
                System.out.println("Easy Four");
                break;
              case 2:
                System.out.println("Fever Five");
                break;
              case 3:
                System.out.println("Hard Six");
                break;
              case 4:
                System.out.println("Seven Out");
                break;
              case 5:
                System.out.println("Easy Eight");
                break;
              case 6:
                System.out.println("Nine");
                break;
            }
            break;
          case 4:
            switch (die2){
              case 1:
                System.out.println("Fever Five");
                break;
              case 2:
                System.out.println("Easy Six");
                break;
              case 3:
                System.out.println("Seven Out");
                break;
              case 4:
                System.out.println("Hard Eight");
                break;
              case 5:
                System.out.println("Nine");
                break;
              case 6:
                System.out.println("Easy Ten");
                break;
            }
            break;
          case 5:
            switch (die2){
              case 1:
                System.out.println("Easy Six");
                break;
              case 2:
                System.out.println("Seven Out");
                break;
              case 3:
                System.out.println("Easy Eight");
                break;
              case 4:
                System.out.println("Nine");
                break;
              case 5:
                System.out.println("Hard Ten");
                break;
              case 6:
                System.out.println("Yo-leven");
                break;
            }
            break;
          case 6:
            switch (die2){
              case 1:
                System.out.println("Seven Out");
                break;
              case 2:
                System.out.println("Easy Eight");
                break;
              case 3:
                System.out.println("Nine");
                break;
              case 4:
                System.out.println("Easy Ten");
                break;
              case 5:
                System.out.println("Yo-leven");
                break;
              case 6:
                System.out.println("Boxcars");
                break;
            }
            break;
        }
      default: //case for when invalid roll for six sided die in given
        System.out.println("Invalid die roll");
        break;
    }
  }
}