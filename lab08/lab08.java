//Severo Lopez
//08 Nov 2018
//CSE002-311
import java.lang.Math;
public class lab08{
	public static void main(String[] args){
		int[] array1 = new int[100];
		int[] array2 = new int[100];
		int size = array1.length;
		int i = 0;
		int count = 0;
		for (i = 0; i < size; i++){
			array1[i] = (int) (Math.random() * 100);
		}
		System.out.print("Array 1 holds the following integers: " );
		for (i = 0; i < size; i++){
			System.out.print(array1[i] + " ");
		}
		System.out.println();
		for (i = 0; i < size; i++){
			count = array1[i];
			array2[count]++;
		}
		for (i = 0; i < size; i++){
			if (array2[i] == 1){
				System.out.println(i + " occurs " + array2[i] + " time");
			} else if (array2[i] > 1 || array2[i] == 0){
				System.out.println(i + " occurs " + array2[i] + " times");
			}
		}
	}
}