////////////////////
//Severo Lopez
//10 Sep 2018
//CSE002-311
public class Arithmetic {
  // main method required for every Java program
  public static void main(String[] args) {
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants

    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt

    int numBelts = 1; //Number of belts
    double beltCost = 33.99; //cost per belt

    double paSalesTax = 0.06; //the tax rate

    double pantsTotal = numPants*pantsPrice; //Total price of pants
    double shirtsTotal = numShirts*shirtPrice; //Total price of shirts
    double beltsTotal = numBelts*beltCost; //Total price of belts

    double pantsSalesTax = pantsTotal*paSalesTax*100; //Sales tax charged on pants
    pantsSalesTax = (int) pantsSalesTax/100.0; //Eliminates extra digits to the right of decimal point
    double shirtsSalesTax = shirtsTotal*paSalesTax*100; //Sales tax charged on shirts
    shirtsSalesTax = (int) shirtsSalesTax/100.0; //Eliminates extra digits to the right of decimal point
    double beltsSalesTax = beltsTotal*paSalesTax*100; //Sales tax charged on belts
    beltsSalesTax = (int) beltsSalesTax/100.0; //Eliminates extra digits to the right of decimal point

    //Prints total cost of pants and the associated sales tax
    System.out.println("Cost of pants is $"+pantsTotal+" with a sales tax of $"+pantsSalesTax+"");
    //Prints total cost of shirts and the associated sales tax
    System.out.println("Cost of pants is $"+shirtsTotal+" with a sales tax of $"+shirtsSalesTax+"");
    //Prints total cost of belts and the associated sales tax
    System.out.println("Cost of pants is $"+beltsTotal+" with a sales tax of $"+beltsSalesTax+"");

    double totalCost = (pantsTotal+shirtsTotal+beltsTotal); //Total cost of purchases before tax
    double salesTaxTotal = (pantsSalesTax+shirtsSalesTax+beltsSalesTax); //Total sales tax
    double totalWithSalesTax = (totalCost+salesTaxTotal); //Total paid for the transaction, including sales tax

    //Prints total cost of all items before tax
    System.out.println("Total cost before tax is $"+totalCost+"");
    //Prints total sales tax
    System.out.println("Total sales tax is $"+salesTaxTotal+"");
    //Prints total cost including tax
    System.out.println("Total cost including tax is $"+totalWithSalesTax+"");
  }
}

