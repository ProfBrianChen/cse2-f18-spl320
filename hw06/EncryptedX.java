//Severo Lopez
//21 Oct 2018
//CSE002-311
import java.util.Scanner;
public class EncryptedX{
  // main method required for every Java program
  public static void main(String[] args) {
  	Scanner scan = new Scanner(System.in);
  	System.out.print("Square size: ");
  	int input = -1;
    boolean correct = false;
    while (!correct){ //checks to see if the user input an integer
    	if (!scan.hasNextInt()){
    		scan.next();
    		System.out.println("Invalid input. Please enter an integer: ");
    	} else {
    		input = scan.nextInt();
    		if (input < 0 || input > 100) { //checks to see if the user input a negative integer
    			System.out.print("Invalid input. Please enter an integer between 0 and 100: ");
    		} else {
    			correct = true;
    		}
    	}
    }
    for (int row = 1; row < input + 1; row++) { //for loop to write the necessary output for each row
        for (int col = 1; col < input + 1; col++) { //nested for loop to assign a blank space or * for every space in a row
            if (row == col) { //prints a blank space for the downward facing diagonal
                System.out.print(" ");
            } else if (row + col == input + 1) { //prints a blank space for the upward facing diagonal
            	System.out.print(" ");
            } else {  //prints a * for everything else
                System.out.print("*");
            }
        }
        System.out.println(); //starts next line
    }
  }
}