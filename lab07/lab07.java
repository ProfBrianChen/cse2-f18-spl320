//Severo Lopez
//25 Oct 2018
//CSE002-311
import java.util.Random;
import java.util.Scanner;
public class lab07{
  // main method required for every Java program
  public static String adjectives(int adjective){
  	switch (adjective){
  		case 1:
  			return "red";
  		case 2:
  			return "yellow";
  		case 3:
  			return "blue";
  		case 4:
  			return "green";
  		case 5:
  			return "orange";
  		case 6:
  			return "purple";
  		case 7:
  			return "pink";
  		case 8:
  			return "brown";
  		case 9:
  			return "white";
  		case 10:
  			return "black";
  	}
  	return "aight";
  }
  public static String subject(int subject){
  	switch (subject){
  		case 1:
  			return "fox";
  		case 2:
  			return "dog";
  		case 3:
  			return "cat";
  		case 4:
  			return "fish";
  		case 5:
  			return "shark";
  		case 6:
  			return "octopus";
  		case 7:
  			return "bear";
  		case 8:
  			return "bat";
  		case 9:
  			return "bunny";
  		case 10:
  			return "rabbit";
  	}
  	return "aight";
  }
  public static String verb(int verb){
  	switch (verb){
  		case 1:
  			return "flew over";
  		case 2:
  			return "jumped over";
  		case 3:
  			return "swam over";
  		case 4:
  			return "ate";
  		case 5:
  			return "bumped into";
  		case 6:
  			return "pushed";
  		case 7:
  			return "dabbed on";
  		case 8:
  			return "ate with";
  		case 9:
  			return "fed";
  		case 10:
  			return "lived with";
  	}
  	return "aight";
  }
  public static String object(int object){
  	switch (object){
  		case 1:
  			return "car";
  		case 2:
  			return "plane";
  		case 3:
  			return "train";
  		case 4:
  			return "bicycle";
  		case 5:
  			return "scooter";
  		case 6:
  			return "skateboard";
  		case 7:
  			return "unicycle";
  		case 8:
  			return "tricycle";
  		case 9:
  			return "submarine";
  		case 10:
  			return "human";
  	}
  	return "aight";
  }
  public static void main(String[] args) {
  	Random randomGenerator = new Random();
  	int randomInt = randomGenerator.nextInt(10) + 1;
  	System.out.println("The " + adjectives(randomInt) + " " + subject(randomInt) + " " + verb(randomInt) + " the " + object(randomInt));
  	Scanner scan = new Scanner(System.in);
  	System.out.print("Print another sentence (1 for yes, 0 for no): ");
  	int input = -1;
    boolean correct = false;
    while (!correct){ //checks to see if the user input an integer
    	if (!scan.hasNextInt()){
    		scan.next();
    		System.out.println("Invalid input. Please enter an integer: ");
    	} else {
    		input = scan.nextInt();
    		if (input < 0 || input > 1) { //checks to see if the user input a negative integer
    			System.out.print("Invalid input. Please enter 1 or 0: ");
    		} else if (input ==1) {
    			randomInt = randomGenerator.nextInt(10) + 1;
    			System.out.println("The " + adjectives(randomInt) + " " + subject(randomInt) + " " + verb(randomInt) + " the " + object(randomInt));
    			System.out.println("Print another sentence (1 for yes, 0 for no)?: ");
    		} else if (input ==0) {
    			correct = true;
    		}
    	}
    }
   	int randomInt2 = randomGenerator.nextInt(10) + 1;
   	boolean same = false;
   	while (!same) {
	   	if (randomInt2 == randomInt){
	   		randomInt2 = randomGenerator.nextInt(10) + 1;
	   	} else {
	   		same = true;
	   	}
   	}
    System.out.println("It used the " + subject(randomInt2) + " to " + verb(randomInt2) + " the " + object(randomInt2));
    int randomInt3 = randomGenerator.nextInt(10) + 1;
   	boolean same2 = false;
   	while (!same2) {
	   	if (randomInt3 == randomInt2){
	   		randomInt3 = randomGenerator.nextInt(10) + 1;
	   	} else if (randomInt3 == randomInt) {
	   		randomInt3 = randomGenerator.nextInt(10) + 1;
	   	} else {
	   		same2 = true;
	   	}
   	}
   	System.out.println("That " + subject(randomInt) + " " + verb(randomInt3) + " the " + object(randomInt3));
  }
}