//Severo Lopez
//15 Nov 2018
//CSE002-311
public class lab09{
	public static int[] copy(int[] array){
		int[] array0 = new int[array.length];
		for (int i = 0; i < array.length; i++){
			array0[i] = array[i];
		}
		return array0;
	}
	public static void inverter(int[] array){
		for (int i = 0; i <= (int) (array.length - 1) / 2; i++){
			int index = array.length - i - 1;
			int reverse = array[i];
			array[i] = array[index];
			array[index] = reverse;
		}
	}
	public static int[] inverter2(int[] array){
		int[] arrayCopy = copy(array);
		inverter(arrayCopy);
		return arrayCopy;
	}
	public static void print(int[] array){
		for (int i = 0; i < array.length; i++){
			System.out.print(array[i] + " ");
		}
	}
	public static void main(String[] args){
		int[] array0 = new int[11];
		for (int i = 0; i < array0.length; i++){
			array0[i] = i;
		}
		int[] array1 = copy(array0);
		int[] array2 = copy(array0);

		inverter(array0);
		print(array0);
		System.out.println();

		inverter2(array1);
		print(array1);
		System.out.println();

		int[] array3 = inverter2(array2);
		print(array3);
		System.out.println();
	}
}