//Severo Lopez
//CSE002-313
//11 November 2018
import java.util.Scanner;
public class Shuffling{ 
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		//Changed original numCards from 0 to 5 to match sample given
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		//Assigns suit and rank to each card in order
		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13]+suitNames[i/13]; 
			//Removed next line from original code because printArray(cards) already does this
			//System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		//Prints cards in order
		printArray(cards); 
		//Shuffles cards
		shuffle(cards); 
		System.out.println();
		System.out.println("Shuffled:");
		//Prints cards in shuffled order
		printArray(cards); 
		System.out.println();
		while(again == 1){ 
			hand = getHand(cards,index,numCards); 
			System.out.println("Hand:");
			//Prints hand of 5 cards
			printArray(hand);
			System.out.println();
			//numCards is removed from the index until there are not enough cards to give full hand
			index = index - numCards;
			//Once enough cards are removed for there to not be a full hand, cards are reshuffled
			if (index < 5){
				index = 51;
				shuffle(cards);
			}
			System.out.println("Enter a 1 if you want another hand drawn"); 
			again = scan.nextInt(); 
		}  
	}
	public static void shuffle(String[] cards){
		//Shuffles deck of cards
		for (int i = 0; i < cards.length; i++){
			//Index is assigned to random spot in deck
			int index = (int) (Math.random() * cards.length);
			//Switch card at i with card at index
			String shuffled = cards[i];
			cards[i] = cards[index];
			cards[index] = shuffled;
		}
	}
	public static String[] getHand(String[] cards, int index, int numCards){
		String[] hand = new String[numCards];
		//Takes five cards from the end of the inputted array and returns as hand
		for (int i = 0; i < numCards; i++){
			hand[i] = cards[index - numCards + i];
		}
		return hand;
	}
	public static void printArray(String[] cards){
		//Prints array of cards
		for (int i = 0; i < cards.length; i++){
			System.out.print(cards[i] + " ");
		}
	}
}