//Severo Lopez
//30 Oct 2018
//CSE002-311
import java.util.Scanner;
public class hw07{
	public static String sampleText(){
		//user inputs text and the text is printed out
		Scanner scan = new Scanner(System.in); 
		System.out.println("Enter a sample text: ");
		String text = scan.nextLine();
		System.out.println();
		System.out.println("You entered: " + text);
		return text;
	}

	public static char printMenu(String text){
		//user inputs a char to call a specific method
		Scanner scan = new Scanner(System.in);
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
		System.out.println();
		System.out.println("Choose an option:");
	    char input = ' ';
	    //checks to see if the char inputted is able to call a method, if not it prompts the user to input a proper char
	    boolean correct = false;
	    while (!correct){
	    	if (!scan.hasNext()){
	    		scan.next().charAt(0);
	    		System.out.println("Invalid input. Please enter a proper input: ");
	    	} else {
	    		input = scan.next().charAt(0);;
	    		if (input == 'q'){ //exit menu
					correct = true;
	    		} else if (input == 'c'){ //call getNumOfNonWSCharacters
	    			getNumOfNonWSCharacters(text);
	    		} else if (input == 'w'){ //call getNumOfWords
	    			getNumOfWords(text);
	    		} else if (input == 'f'){ //prompt user to input a word to find, call findText
	    			Scanner scanFind = new Scanner(System.in);
				    System.out.println("Enter a word or phrase to be found: ");
				    String wordFind = scanFind.nextLine();
	    			findText(text, wordFind);
	    		} else if (input == 'r'){ //call replaceExclamation
	    			replaceExclamation(text);
	    		} else if (input == 's'){ //call shortenSpace, print edited text
	    			String s = shortenSpace(text);
	    			System.out.println("Edited text: " + s);
	    			System.out.println();
	    		} else {
	    			System.out.print("Invalid input. Please enter a proper input: ");
	    		}

	    	}
	    }
	    return input;
	}

	public static int getNumOfNonWSCharacters(String textNumChar){
		int textLength = textNumChar.length();
		int wsCount = 0;
	   	//goes through the text one character at a time looking for characters that are white spaces
	   	while(textNumChar.contains(" ")){
	   		//when a white space is encountered, wsCount goes up 1, substring is created that starts after the index of the white space
	   		wsCount++;
	      	textNumChar = textNumChar.substring(textNumChar.indexOf(" ")+1);
	   	}
	   	int charCount = textLength - wsCount;
	   	System.out.println("Number of non-whitespace characters: " + charCount);
		System.out.println();
		return charCount;
	}

	public static int getNumOfWords(String textNumWords){
		int wordCount = 0;
		int end = textNumWords.length();
		//goes through the text one character at a time to check if the character at i is a letter or not
		for (int i = 0; i < end; i++){
			//if the character at i is a letter, the character isn't the end of the text, and the character after i is not a letter, the count goes up 1
			if (Character.isLetter(textNumWords.charAt(i)) && i != end && !Character.isLetter(textNumWords.charAt(i+1))){
				wordCount++;
			} 
			//if the character at i is an apostrophe, the count goes down 1 (to take into consideration contractions and possession words which would count as two words using the above method)
			else if (textNumWords.indexOf('\'') == i){ 
				wordCount--;
			}
		}
		System.out.println("Number of Words: " + wordCount);
		System.out.println();
		return wordCount;
	}

	public static int findText(String textFind, String find){
	    int instances = 0;
	    //searches text for word of interest
	    while (textFind.contains(find)){ 
	    	instances++;
	    	//creates a substring that starts after the last character of the word or phrase being counted
	    	textFind = textFind.substring(textFind.indexOf(find) + find.length()); 
	    }
	    System.out.println("\"" + find + "\" instances: " + instances);
		System.out.println();
	    return instances;
	}

	public static String replaceExclamation(String textReplaceExclamation){
		String newText = "";
		int end = textReplaceExclamation.length();
		//searches for exclamation marks in text one character at a time
		for (int i = 0; i < end; i++){
			//checks if the the character at i is an exclamation mark
			if (textReplaceExclamation.charAt(i) == '!'){ 
				//rewrites text without exclamation mark and writes newText to save edited text
				textReplaceExclamation = textReplaceExclamation.substring(0,i) + '.' + textReplaceExclamation.substring(i+1);
				newText = textReplaceExclamation.substring(0,i) + '.' + textReplaceExclamation.substring(i+1);
			}
		}
		System.out.println("Edited text: " + newText);
		System.out.println();
		return newText;
	}

	public static String shortenSpace(String textShortenSpace){
		//goes through text one character at a time and modifies the end so that it matches with each rewritten text after a double space is fixed
		for (int i = 0; i < textShortenSpace.length() - 2; i++){
			//searches for instances in text where there is a space at character i followed by a space at character i + 1
			if (textShortenSpace.charAt(i) == ' ' && textShortenSpace.charAt(i + 1) == ' '){
				//rewrites text by splicing the text up to the first space and after the second space
				textShortenSpace = textShortenSpace.substring(0,i) + ' ' + textShortenSpace.substring(i+2);
			}
		}
		return textShortenSpace;
	}

	//main method required for every Java program
	public static void main(String[] args){
		String text = sampleText();
		System.out.println();
		//calls printMenu
		printMenu(text);
	}
}