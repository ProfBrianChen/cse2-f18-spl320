//////////////
//// CSE 02 Welcome Class
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints Welcome spl320 to terminal window
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-S--P--L--3--2--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My nickname is veto. I enjoy long walks on the beach.");
    
  }
  
}