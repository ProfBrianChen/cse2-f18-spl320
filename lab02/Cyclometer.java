//Severo Lopez
//6 Sep 2018
//CSE002-311
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480;  // Time of Trip 1 as an integer
      int secsTrip2=3220;  // Time of Trip 2 as an integer
      int countsTrip1=1561;  // Rotations of Trip 1 as an integer
      int countsTrip2=9037; // Rotations of Trip 2 as an integer
      double wheelDiameter=27.0,  // Diameter of wheel
      PI=3.14159, // constant pi
      feetPerMile=5280,  // Number of per mile
      inchesPerFoot=12,   // Number of inches per foot
      secondsPerMinute=60;  // Number of seconds per minute
      double distanceTrip1,distanceTrip2,totalDistance;  
        // creates variables for the distances of Trip 1 and 2 and the total distance
	  System.out.println("Trip 1 took "
                       +(secsTrip1/secondsPerMinute)+
                       " minutes and had "+countsTrip1+
                       " counts.");
	  System.out.println("Trip 2 took "
                       +(secsTrip2/secondsPerMinute)+
                       " minutes and had "+countsTrip2+
                       " counts.");
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
      //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    }  //end of main method   
} //end of class




