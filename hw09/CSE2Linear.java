//Severo Lopez
//25 Nov 2018
//CSE002-311
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
	public static int linearSearch(int[] list, int key){
		//Goes through each element of array to find key
		int count = 1;
		for (int i = 0; i < list.length; i++){
			if (list[i] == key){
				return count;
			}
			count++;
		}
		return -1;
	}
	public static int binarySearch(int[] list, int key){
		//Used Prof. Carr's code for binary search
		int count = 1;
		int low = 0;
		int high = list.length - 1;
		//Splits array in half and searches bottom or upper half based on key's position and keeps repeating until it finds key
		while (high >= low){
			int mid = (low + high) / 2;
			if (key < list[mid]){
				high = mid - 1;
			} else if (key == list[mid]){
				return count;
			} else {
				low = mid + 1;
			}
			count++;
		}
		return 0;
	}
	public static void scramble(int[] list){
		//Shuffles elements by picking a random spot in array and swapping the element at i with it
		for (int i = 0; i < list.length; i++){
			int index = (int) (Math.random() * list.length);
			int shuffled = list[i];
			list[i] = list[index];
			list[index] = shuffled;
		}

	}
	public static void print(int[] list){
		for (int i = 0; i < list.length; i++){
			System.out.print(list[i] + " ");
		}
	}
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
	  	System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
	  	int inputPrevious = -1;
	    int[] list = new int[15];
	    for (int i = 0; i < 15; i++){    
		    boolean correct = false;
		    while (!correct){ //checks to see if the user input an integer
		    	if (!scan.hasNextInt()){
		    		scan.next();
		    		System.out.println("Invalid input. Please enter an integer: ");
		    	} else {
		    		list[i] = scan.nextInt();
		    		if (list[i] < 0 || list[i] > 100) { //checks to see if the user input a negative integer
		    			System.out.println("Invalid input. Please enter an integer between 0 and 100: ");
		    		} else if (list[i] < inputPrevious){
		    			System.out.println("Invalid input. Please enter an integer that is greater than the previously entered ones: ");
		    		} else {
		    			correct = true;
		    		}
		    	}
		    }
		    inputPrevious = list[i];
		}
		System.out.println("Grades:");
		print(list);
		System.out.println();
		System.out.print("Enter a grade to search for: ");
		int input = -1;
    	boolean correct2 = false;
	    while (!correct2){ //checks to see if the user input an integer
	    	if (!scan.hasNextInt()){
	    		scan.next();
	    		System.out.println("Invalid input. Please enter an integer: ");
	    	} else {
	    		input = scan.nextInt();
	    		if (input < 0 || input > 100) { //checks to see if the user input a negative integer
	    			System.out.print("Invalid input. Please enter an integer between 0 and 100: ");
	    		} else {
	    			correct2 = true;
	    		}
	    	}
    	}
    	if (binarySearch(list, input) != 0){
    		System.out.println("Grade found. Binary search iterations: " + binarySearch(list, input));
    	} else {
    		System.out.println("Grade not found");
    	}
    	scramble(list);
    	System.out.println("Scrambled grades:");
    	print(list);
    	System.out.println();
		System.out.print("Enter a grade to search for: ");
		int input2 = -1;
    	boolean correct3 = false;
	    while (!correct3){ //checks to see if the user input an integer
	    	if (!scan.hasNextInt()){
	    		scan.next();
	    		System.out.println("Invalid input. Please enter an integer: ");
	    	} else {
	    		input2 = scan.nextInt();
	    		if (input2 < 0 || input2 > 100) { //checks to see if the user input a negative integer
	    			System.out.print("Invalid input. Please enter an integer between 0 and 100: ");
	    		} else {
	    			correct3 = true;
	    		}
	    	}
    	}
    	linearSearch(list, input2);
    	if (linearSearch(list, input2) == -1){
    		System.out.println("Grade not found");
    	} else {
    		System.out.println("Grade found. Linear search iterations: " + linearSearch(list, input2));
    	}
	}
}