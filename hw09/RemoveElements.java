//Severo Lopez
//26 Nov 2018
//CSE002-311
import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
	public static void main(String [] arg){
		//Code for main method given in homework description
        Scanner scan=new Scanner(System.in);
		int num[]=new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		String answer="";
		do{
			System.out.println("Random input 10 ints [0-9]");
			num = randomInput();
			String out = "The original array is: ";
			out += listArray(num);
			System.out.println(out);
			
			System.out.print("Enter the index: ");
			index = scan.nextInt();
			newArray1 = delete(num,index);
			String out1="The output array is: ";
			out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
			System.out.println(out1);
			
			System.out.print("Enter the target value: ");
			target = scan.nextInt();
			newArray2 = remove(newArray1,target);
			String out2="The output array is: ";
			out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
			System.out.println(out2);
			
			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
			answer=scan.next();
		}while(answer.equals("Y") || answer.equals("y"));
	}
	public static String listArray(int num[]){
		//Creates a string of the from "{2, 3, -9}"
		String out="{";
		for(int j=0;j<num.length;j++){
			if(j>0){
				out+=", ";
			}
			out+=num[j];
		}
		out+="} ";
		return out;
	}
	public static int[] randomInput(){
		//Generates a string of ten random numbers between 0 and 9
		int array[] = new int[10];
		for (int i = 0; i < array.length; i++){
			array[i] = (int) (Math.random() * array.length);
		}
		return array;
	}
	public static int[] delete(int[] list, int pos){
		//Creates a string with one less element than the inputted array, 
		//with the element lost being the elemnt at the inputted position
		int array[] = new int[list.length - 1];
		for (int i = 0; i < array.length; i++){
			//Everything before the inputted position is the same as the 
			//original array
			if (i < pos){
				array[i] = list[i];
			//Every element at the inputted position and after is equal to
			//the element one position greater in the inputted array
			} else if (i >= pos){
				array [i] = list[i + 1];
			}
		}
		return array;
	}
	public static int[] remove(int[] list, int target){
		//Counts the instances of the target in the inputted array
		int count = 0;
		for (int i = 0; i < list.length; i++){
			if (list[i] == target){
				count++;
			}
		}
		//Creates an array with count less elements than the inputted
		//array
		int array[] = new int[list.length-count];
		int count2 = 0;
		for (int i = 0; i < list.length; i++){
			//Copies the elements from list[i] to array[count2] where
			//array[count2] increases with the for loop (i++) for every 
			//element unless that element in list[i] is equal to target
			if (list[i] != target){
				array[count2] = list[i];
				count2++;
			}
		}
		return array;
	}
}