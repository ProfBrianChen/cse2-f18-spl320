//Severo Lopez
//19 Sep 2018
//CSE002-311
public class CardGenerator{
  // main method required for every Java program
  public static void main(String[] args) {
    int randomNumber = (int) (Math.random() * 52); //generates a random number between 1 and 52
    if (randomNumber < 14){ //Cards that are diamonds
      switch (randomNumber){
        case 1: //case for randomNumber = 1
          System.out.println("You picked the Ace of Diamonds"); //Designates 1 as ace of diamonds and prints statement
          break; //separates cases so only one statement is printed out
        case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: 
          System.out.println("You picked the " + randomNumber + " of Diamonds"); //prints out random number + suit
          break;
        case 11:
          System.out.println("You picked the Jack of Diamonds"); //designates 11 as jack of diamonds
          break;
        case 12:
          System.out.println("You picked the Queen of Diamonds"); //designates 12 as queen of diamonds
          break;
        case 13:
          System.out.println("You picked the King of Diamonds"); //designates 13 as king of diamonds
          break;
      }
    }
    else if (randomNumber > 13 & randomNumber < 27){ //Cards that are clubs
      switch (randomNumber){
        case 14:
          System.out.println("You picked the Ace of Clubs"); //designates 14 as ace of clubs
          break;
        case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23:
          System.out.println("You picked the " + (randomNumber % 13) + " of Clubs"); //prints out random number modulo 13 since there are only numeric cards of 2-10
          break;
        case 24:
          System.out.println("You picked the Jack of Clubs"); //designates 24 as jack of clubs
          break;
        case 25:
          System.out.println("You picked the Queen of Clubs"); //designates 25 as queen of clubs
          break;
        case 26:
          System.out.println("You picked the King of Clubs"); //designates 26 as king of clubs
          break;
      }
    }
    else if (randomNumber > 26 & randomNumber < 40){ //Cards that are hearts
      switch (randomNumber){
        case 27:
          System.out.println("You picked the Ace of Hearts"); //designates 27 as ace of hearts
          break;
        case 28: case 29: case 30: case 31: case 32: case 33: case 34: case 35: case 36:
          System.out.println("You picked the " + (randomNumber % 13) + " of Hearts"); //prints out random number minus 26
          break;
        case 37:
          System.out.println("You picked the Jack of Hearts"); //designates 37 as jack of hearts
          break;
        case 38:
          System.out.println("You picked the Queen of Hearts"); //designates 38 as queen of hearts
          break;
        case 39:
          System.out.println("You picked the King of Hearts"); //designates 39 as king of hearts
          break;
      }
    }
    else if (randomNumber > 39){ //Cards that are spades
      switch (randomNumber){
        case 40:
          System.out.println("You picked the Ace of Spades"); //designates 40 as ace of spades
          break;
        case 41: case 42: case 43: case 44: case 45: case 46: case 47: case 48: case 49:
          System.out.println("You picked the " + (randomNumber % 13) + " of Spades"); //prints out random number minus 39
          break;
        case 50:
          System.out.println("You picked the Jack of Spades"); //designates 50 as jack of spades
          break;
        case 51:
          System.out.println("You picked the Queen of Spades"); //designates 51 as queen of spades
          break;
        case 52:
          System.out.println("You picked the King of Spades"); //designates 52 as king of spades
          break;
      }
    }
  }  //end of main method    
} //end of class