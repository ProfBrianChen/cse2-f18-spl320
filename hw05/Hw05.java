//Severo Lopez
//9 Oct 2018
//CSE002-311
import java.util.Scanner; //allow you to use a scanner to ask for input
public class Hw05{
  // main method required for every Java program
  public static void main(String[] args) {
  	Scanner scan = new Scanner(System.in);
  	System.out.print("Hands to generate: ");
    int hands = -1;
    boolean correct = false;
    while (!correct){ //checks to see if the user input an integer
    	if (!scan.hasNextInt()){
    		scan.next();
    		System.out.println("Invalid input. Please enter an integer: ");
    	} else {
    		hands = scan.nextInt();
    		if (hands < 0) { //checks to see if the user input a negative integer
    			System.out.print("Invalid input. Please enter a positive integer: ");
    		} else {
    			correct = true;
    		}
    	}
    }
    int card1, card2, card3, card4, card5;
    int i = 0; //count for while statement that is equivalent to the ith hand
    double fourOfAKind, threeOfAKind, twoPair, onePair;
    fourOfAKind = 0; //amt of each type of hand starts at 0
    threeOfAKind = 0;
    twoPair = 0;
    onePair = 0;
    while (i < hands){ //while statement that goes through all i hands
    	card1 = (int) (Math.random() * 52) + 1; //randomly generates a number equivalent to one from a deck of 52 cards
	    card2 = (int) (Math.random() * 52) + 1;
	    card3 = (int) (Math.random() * 52) + 1;
	    card4 = (int) (Math.random() * 52) + 1;
	    card5 = (int) (Math.random() * 52) + 1;
	    boolean different = false;
	    while (!different){ //checks to make sure that none of the cards are the same
	    	if (card1 == card2){
	    		card2 = (int) (Math.random() * 52) + 1;
	    	} else if (card1 == card3){
	    		card3 = (int) (Math.random() * 52) + 1;
	    	} else if (card1 == card4){
	    		card4 = (int) (Math.random() * 52) + 1;
	    	} else if (card1 == card5){
	    		card5 = (int) (Math.random() * 52) + 1;
	    	} else if (card2 == card3 || card2 == card4 || card2 == card5){
	    		card2 = (int) (Math.random() * 52) + 1;
	    	} else if (card3 == card4 || card3 == card5){
	    		card3 = (int) (Math.random() * 52) + 1;
	    	} else if (card4 == card5){
	    		card4 = (int) (Math.random() * 52) + 1;
	    	} else {
	    		different = true;
	    	}
	    }

	    card1 = card1 % 13; //makes all cards an integer between 1 and 13
	    card2 = card2 % 13;
	    card3 = card3 % 13;
	    card4 = card4 % 13;
	    card5 = card5 % 13; 

	    if (card1 == card2 && card1 == card3 && card1 == card4 || card1 == card2 && card1 == card3 && card1 == card5 ||
	    	card1 == card3 && card1 == card4 && card1 == card5 || card1 == card2 && card1 == card4 && card1 == card5 ||
	    	card2 == card3 && card1 == card4 && card1 == card5){ //if statement to see if hand has a four of a kind
	    	fourOfAKind++;
	    } else if (card1 == card2 && card1 == card3 || card1 == card2 && card1 == card4 || card1 == card2 && card1 == card5 || 
	    	card1 == card3 && card1 == card4 || card1 == card3 && card1 == card5 || card1 == card4 && card1 == card5 || 
	    	card2 == card3 && card2 == card4 || card2 == card3 && card2 == card5 || card2 == card4 && card2 == card5 || 
	    	card3 == card4 && card3 == card5){ //if statement to check if the hand has a three of a kind
	    	threeOfAKind++;
	    } else if (card1 == card2 && card3 == card4 || card1 == card2 && card3 == card5 || 
	    	card1 == card2 && card4 == card5 || card1 == card3 && card2 == card4 || 
	    	card1 == card3 && card2 == card5 || card1 == card3 && card4 == card5 || 
	    	card1 == card4 && card2 == card3 || card1 == card4 && card2 == card5 || 
	    	card1 == card4 && card3 == card5 || card1 == card5 && card2 == card3 || 
	    	card1 == card5 && card2 == card4 || card1 == card5 && card3 == card4 || 
	    	card2 == card3 && card4 == card5 || card2 == card4 && card3 == card5 || 
	    	card2 == card5 && card3 == card4 || card3 == card4 && card2 == card5){ //if statement to check if the hand has two pairs
	    	twoPair++;
	    } else if (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5 || 
	    	card2 == card3 || card2 == card4 || card2 == card5 || card3 == card4 || 
	    	card3 == card5 || card4 == card5){ //if statement to check if the hand has only one pair
	    	onePair++;
	    }
    	i++; //increases count and goes to next hand
    }
    System.out.println("The number of loops: " + hands);


    double p4 = fourOfAKind / hands; //probability of each type of hand
    double p3 = threeOfAKind / hands;
    double p2 = twoPair / hands;
    double p1 = onePair / hands;

    System.out.print("The probability of a Four-of-a-kind: ");
    System.out.printf("%.3f\n", p4);
    
    System.out.print("The probability of a Three-of-a-kind: ");
    System.out.printf("%.3f\n", p3);
    
    System.out.print("The probability of a Two-pair: ");
    System.out.printf("%.3f\n", p2);
    
    System.out.print("The probability of a One-pair: ");
    System.out.printf("%.3f\n", p1);
  }
}