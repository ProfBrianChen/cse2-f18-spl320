//Severo Lopez
//16 Sep 2018
//CSE002-311
import java.util.Scanner; //imports scanner for java to read
public class Pyramid{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //declares scanner
    System.out.print("The square side of the pyramid is (input length): "); //asks user to input the length of the sides of a square base
    double length = myScanner.nextDouble(); //variable for the length of the sides of a square base as scanner
    System.out.print("The height of the pyramid is (input height): "); //asks user to input the height
    double height = myScanner.nextDouble(); //variable for the height as scanner
    double volume = ((length)*(length)*(height))/3; //equation using the length of the sides and height to find the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + volume); //displays the volume inside the pyramid

  }  //end of main method    
} //end of class