//Severo Lopez
//16 Sep 2018
//CSE002-311
import java.util.Scanner; //imports scanner for java to read
public class Convert{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //declares scanner
    System.out.print("Enter the affected area in acres: "); //asks user to input area affected in acres
    double area = myScanner.nextDouble(); //variable for area as a scanner
    System.out.print("Enter the rainfall in the affected area: "); //asks user to input amount of rain in incehs
    double rain = myScanner.nextDouble(); //variable for rain as a scanner
    double cubicMiles = ((area)*(0.0015625))*((rain)*(1.5783e-5)); //converts acres (area) to square miles and inches (rain) to miles to find cubic miles
    System.out.println(cubicMiles + " cubic miles"); //displays cubic miles of rain

  }  //end of main method    
} //end of class